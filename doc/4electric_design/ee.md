## 电控
![](https://i0.wp.com/www.linuxlinks.com/wp-content/uploads/2019/05/electronic-design-automation-tools.jpg?resize=700%2C370&ssl=1)

目前分为硬件和软件工程师
![](https://www.ti.com/content/dam/ticom/images/applications/custom-boards/c2000-hardware-lab.jpg)

![](https://damassets.autodesk.net/content/dam/autodesk/draftr/19187/ed2.png)
![](https://gitlab.com/picbed/bed/uploads/4b48516c6d9ce4f0cf1b66ae50a62d14/pcb-2-layer-printed-circuit-board-concepts-1.jpg)
![](https://gitlab.com/picbed/bed/uploads/b20ea51f5da79c03e011bdb8d3879eb3/WX20200519-151704.png)

#### 0基本原理
* U=I*R
* P=U*I
* 交流电&直流电
* 串联&并联


[基本原理](https://www.nexmaker.com/doc/4electric_design/electricparameter_component.html)

#### 1. 硬件
###### 1.1PCB
![](https://gitlab.com/picbed/bed/uploads/4b48516c6d9ce4f0cf1b66ae50a62d14/pcb-2-layer-printed-circuit-board-concepts-1.jpg)
[参考链接](https://www.nexmaker.com/doc/4electric_design/basicknowledge.html)
###### 1.2 开源硬件

###### 1.3元器件
![](https://gitlab.com/picbed/bed/uploads/e48485a5f5aff592d4702750b250b6f0/shutterstock_1238283463.jpg)
![](https://www.caraudiohelp.com/images/resistor_chart.png)
![](https://semiwiki.com/wp-content/uploads/2020/11/Top-Semiconductor-Revenue-2020-1024x776.jpg)

###### 1.4 工具
![](https://cdn3.careeraddict.com/uploads/article/59566/illustration-tools.png)
![](https://cdn0.careeraddict.com/uploads/article/62085/problem-solving-engineer.jpg)

![](https://www.milwaukeetool.jobs/-/media/HR-Site/Career-Areas/Engineering/eng-specialty-pgs/Intro-general2.jpg?h=330&w=447&la=en&hash=3A72FCA34D3B11EAB10C2CF635FBF4AE)
![](![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20230804-200046.png))
###### 1.5 单位
测量单位mil（1mil=1/1000inch=0.0254mm，1mm=39.37mil）
###### 1.6 设计工具
![](https://pic4.zhimg.com/v2-6f8d2498b686dbf4b96f143a7b53bc73_r.jpg)
[参考链接](https://zhuanlan.zhihu.com/p/400534464)
![](https://circuitdigest.com/sites/default/files/field/image/EDA-and-Simulation-Softwares.jpg)
![](https://damassets.autodesk.net/content/dam/autodesk/draftr/19187/cropped-1684800927.jpg)


#### 2. 软件
![](https://www.nexmaker.com/doc/5arduino/open_source.html)