# Auto CAD 
## 1. What is Auto CAD
AutoCAD® is computer-aided design (CAD) software that architects, engineers, and construction professionals rely on to create precise 2D and 3D drawings.

Draft, annotate, and design 2D geometry and 3D models with solids, surfaces, and mesh objects
Automate tasks such as comparing drawings, addings blocks, creating schedules, and more
Customize with add-on apps and APIs
Reference from [AutoCAD](https://www.autodesk.com/products/autocad/overview?plc=ACDIST&term=1-YEAR&support=ADVANCED&quantity=1)

![](https://gitlab.com/picbed/bed/uploads/05f7c9b4c1ff67357f62df448f8970fe/620e0b2bed607cdfc94715d6f6b15b59.jpg
)
Reference from [51cto](https://blog.51cto.com/jettcai/2286072)
## 2. How to use it 
The UI of autoCAD2018
![](https://gitlab.com/picbed/bed/uploads/38a5adf67eed15762f3d698b96283c52/WX20200422-055038_2x.png)
Some official learn information for reference:[link](https://knowledge.autodesk.com/support/autocad/learn?sort=score)

We can use graphic commands or Command Line to use the function we need.If we want to use command line we need remember AutoCAD shortcuts & hotkey which you  can reference part 3 "3. AutoCAD shortcuts & hotkey guide"
![](https://gitlab.com/picbed/bed/uploads/ad6b7611fe7d4cc746a651061354af76/command.png
)
## 3. AutoCAD shortcuts & hotkey guide


|type|Hotkey       |   Function | 
| ------------- |------------ | -----:|	
|Drawing|
|Drawing |A     |  Draw an arc|
| Drawing|AR| Opens array dialogue box|  	
| Drawing|F  |FILLET / Rounds and fillets the edges of objects| 
| Drawing|  H|HATCH / Fills an enclosed area or selected objects with a hatch pattern, solid fill, or gradient fill| 
| Drawing| I |INSERT / Inserts a block or drawing into the current drawing| 
| Drawing| L |LINE / Creates straight line segments| 
| Drawing|  MI|MIRROR / Creates a mirrored copy of selected objects| 
| Drawing| O |OFFSET / Creates concentric circles, parallel lines, and parallel curves| 
| Drawing| TA |TEXTALIGN / Aligns multiple text objects vertically, horizontally, or obliquely| 
| Drawing|TB  |TABLE / Creates an empty table object| 
|Editting||| 
|Editting|B |BLOCK / Creates a block definition from selected objects| 
|Editting|X |EXPLODE / Breaks a compound object into its component objects.| 
|Editting|Ctrl+A|Select all objects| 
|Editting|Ctrl+X|| 
|Editting|Ctrl+C|| 
|Editting|Ctrl+V|| 
|Editting|Ctrl+Z|| 
|Editting|DI|DIST / Measures the distance and angle between two points| 
|Editting|E|ERASE / Removes objects from a drawing| 
|Editting|EX|EXTEND / Extends objects to meet the edges of other objects| 
|Editting|M|MOVE / Moves objects a specified distance in a specified direction| 
|Editting|MA|MATCHPROP / Applies the properties of a selected object to other objects| 
|Manage|| 
|Manage|Ctrl+N|New Drawing| 
|Manage|Ctrl+O|Open drawing|
|Manage|Ctrl+S|Save drawing| 
|Manage|RE|REGEN / Regenerates the entire drawing from the current viewport| 
|Manage|REA|REGENALL / Regenerates the drawing and refreshes all viewports| 
|Manage|Ctrl+A|Select all objects| 
|Manage|Ctrl+X|Cut object| 
|Manage|Ctrl+C|Copy object| 
|Manage|Ctrl+V|Paste object| 
|Manage|Ctrl+Z|Undo last action| 
|Manage|ESC|Cancel current command| 
|Manage|D|DIMSTYLE / Creates and modifies dimension styles| 
|Manage|G|GROUP / Creates and manages saved sets of objects called groups| 
|Manage|LA|LAYER / Manages layers and layer properties| 





![](https://gitlab.com/picbed/bed/uploads/a7fb299fc8c8eb4e76f6962fc28afe00/SHORTCUT.png)

* More detail from [AutoCAD](https://www.autodesk.com/shortcuts/autocad)

* [How to create custom command shortcuts in AutoCAD](https://knowledge.autodesk.com/support/autocad/learn-explore/caas/sfdcarticles/sfdcarticles/Creating-a-keyboard-shortcut-in-AutoCAD.html)

## 4. Install
Reference here to install [AutoCAD 2018](http://www.downza.cn/soft/270697.html) , for another version of CAD ,the method is similar. 


## 5. Input fusion data to AutoCAD


* Create standard 2D drawings from your 3D geometry by entering the DRAWING workspace. See the Drawings section of the help for more information.
* Choice Drawing>from design
* In creating drawing,we can determine the size and other information , I choice IOS standard, unit is mmsheet size A4,etc
* Output from dxf formate and then open in AutoCAD  

## 6. Reference
* [AutoCAD](https://www.autodesk.com/products/autocad/overview?plc=ACDIST&term=1-YEAR&support=ADVANCED&quantity=1)
* [AutoCAD hotkey](https://www.autodesk.com/shortcuts/autocad)
* [different autocad from 51cto](https://blog.51cto.com/jettcai/2286072)
* [Official learn information](https://knowledge.autodesk.com/support/autocad/learn?sort=score)
* [CAXA](http://www.caxa.com/)