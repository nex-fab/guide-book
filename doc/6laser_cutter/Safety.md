



## 1. Laser safety
Most types of lasers operating in the visible and near infrared regions are sufficiently intense as to represent a hazard to the eye. Although damage may be caused to all parts of the eyes, the most vulnerable part is the retina, on which the beam may be focussed by the eye lens resulting in the destruction of tissues and the creation of permanent blind spots. High power lasers can also damage the skin. Only trained peaple can operate the machine, and we must close the enclosure before run the machine.

## 2. Laser classes
The current laser classes in national laser classes are: 1, 2, 3A, 3B and 4. The class of each laser must be marked clearly by the supplier. 

*  Class 1:It is safe in the reasonable working condition, and no specific safety controls are required.
*  Class 2: Wavelength range from 400nm to 700nm, visible lasers
* Class 3A: Lower risk than 3B, but direct viewing of beam usually safe
* Class 3B: Intrabeam viewing not safe to the eye, diffusely reflected beam usually safe to the eye, assumed to be safe to the skin.
* Class 4: Those that generate the hazardous diffusely reflected beam, which may be harmful to the eye or skin, and can present a fire hazard.

During normal operation, the machine is enclosed and laser classification is Class 1. In normal usage the interlocked machine does not allow exposure to any dangerous laser radiation. The cutting chamber is fully enclosed during standard operation and keys are needed to access all body panels that open.
[In school's safety rule](http://www.nottingham.edu.cn/en/ehs/laser-safety.aspx),laser operator need wear normal PPE (Laboratory coat, Safety glasses, Dust mask).



## 3. Reference 
* [Nottingham laser safety](https://www.nottingham.edu.cn/en/ehs/laser-safety.aspx)