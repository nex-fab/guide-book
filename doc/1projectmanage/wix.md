## 1.DESCRIPTION

<div align=center>![](https://gitlab.com/picbed/bed/uploads/8877b2dc19a1b09395c2f0f49c93d779/wixlogo.png)

[Wix.com Ltd.](https://www.wix.com/)  is an Israeli software company, providing cloud-based web development services. It allows users to create HTML5 websites and mobile sites through the use of online drag and drop tools. Along with its headquarters and other offices in Israel, Wix also has offices in Brazil, Canada, Germany, India, Ireland, Lithuania, the United States, and Ukraine.Users can add social plug-ins, e-commerce, online marketing, contact forms, e-mail marketing, and community forums to their web sites using a variety of Wix-developed and third-party applications. The Wix website builder is built on a freemium business model, earning its revenues through premium upgrades.

Users must purchase premium packages to connect their sites to their own domains, remove Wix ads, access the form builder, add e-commerce capabilities, or buy extra data storage and bandwidth.Wix provides customizable website templates and a drag-and-drop HTML5 web site builder that includes apps, graphics, image galleries, fonts, vectors, animations and other options. Users also may opt to create their web sites from scratch. In October 2013, Wix introduced a mobile editor to allow users to adjust their sites for mobile viewing.

## 2.Method

1.Sign up for a free wix account
* go to the Wix.com sign up page.
* Register with your email address,Facebook account or Google credentials.
* Click Sign Up and you're all set.
![](https://gitlab.com/picbed/bed/uploads/b783f7c8569b9c56376a375f4f8faa08/wix01.png)

2.Choose a tool to creat your website.Wix ADI is  easier to operate than wix editor,but has less free editing and cannot create a richer web interface.
![](https://gitlab.com/picbed/bed/uploads/6878484cb6e7606b78669178ccd8a541/wix02.png)

3.Choosing the Best Template for Your Site.All the templates are organized into categories for easy browsing. You’ll see: Business and Services, Store, Creative, Community and Blog. Hover over each one to discover subcategories
* Go to the Templates Page.
* Find the category that best fits your business or website idea.
* Select a subcategory if relevant.
* Click View to preview a template or click Edit to start editing
![](https://gitlab.com/picbed/bed/uploads/8ee714882d77f521d492e014170b8582/wix03.gif)

4.Edit website
* Go to your site's dashboard.
* Click Edit Site next to your site's name.
![](https://gitlab.com/picbed/bed/uploads/ae6f246b32fbc15898d0569eb579f160/wix04.webp)

5.Start Adding Elements
The Wix Editor contains hundreds of stunning, customizable elements that you can use to create your site. Choose from images, text, shapes, strips and more to make your site interesting and beautiful!
Click <b>Add</b> on the left side of the Editor to start adding elements
![](https://gitlab.com/picbed/bed/uploads/9a66c2f03769dcb8cf55e911c06bf69f/wix05.gif)

6.Customize Your Elements
You can customize all of the elements on your site.Select an element and click the Settings , Design , Layout   or Animate  icons to customize it. Depending on the element selected, you will see different customization options. Play around with the different settings and options to personalize your elements.
![](https://gitlab.com/picbed/bed/uploads/6e6ea92aae718d11923a3e9ff91abc86/wix06.png)

7.Save, Preview and Publish.You can find the Save, Preview and Publish buttons at the top right of the Editor.
* Save: Click Save regularly so you don't lose any of your work. Keep in mind, saved changes won’t appear on your site until you click Publish.
* Preview: Click Preview to view how your site looks live.
* Publish: Click Publish in the top right corner when you're ready for your site to go live.
![](https://gitlab.com/picbed/bed/uploads/924d5ed2e1820d6b570084d49157ef7e/wix07.png)

The above method from [CNFD- team](https://374737390.wixsite.com/my-site/%E5%89%AF%E6%9C%AC-arduino)
### Reference
* [CNFD- project](https://374737390.wixsite.com/my-site/%E5%89%AF%E6%9C%AC-arduino)
* [wix support](https://support.wix.com/en/?referral=HelpWidgetDashboard)