## 1. Define
[The IoT brings together all kinds of connected devices into a global network of distributed intelligence that opens up a new world of innovation and creativity. Many organizations are already reaping significant benefits by harnessing the data created by large-scale IoT deployments to pursue new opportunities and improve operations.](https://www.arm.com/solutions/iot)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/iot4.png)

![](https://gitlab.com/picbed/bed/uploads/ca95b76b553f29044e0fa6f77ab4b385/main-qimg-f59e32862c8365da5e1c48bb83395802.png)

[reference from quora](https://www.quora.com/What-are-the-top-IoT-companies)


![](https://gitlab.com/picbed/bed/uploads/252f28c87892aa33d94ae34d929cca27/sensors-19-00676-g001.png)



## 2. Platform
* [Alinyun](https://www.aliyun.com/)
* [Autodesk Fusion connect](http://www.autodeskfusionconnect.com/)
* [AWS](https://www.amazonaws.cn/)
* [Azure IoT](https://azure.microsoft.com/zh-cn/overview/iot/) 
* [Gizwits](https://www.gizwits.com/)
* [IBM Watson IoT](https://www.ibm.com/internet-of-things/solutions/iot-platform/watson-iot-platform)
* [Onenet](https://open.iot.10086.cn/)
* [Xiaomi IOT](https://iot.mi.com/new/index.html)
* [Thingspeak](https://thingspeak.com/)
* [Blynk](https://blynk.io/)



