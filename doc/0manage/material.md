#Material

Before your running project ,please check if you have enough material in lab, it would save time in later. We mark the name of material in cabinet.

1. You can use any material in the lab if you now how to use them in safety;
2. If you don't confident with the performance especially safety, you have three method to go ahead:
    * Read guide-document near the material or corresponding machine;
    * Search the keyword of material on our webpage:[NexMaker webpage](https://www.nexmaker.com/),(for example PLA)
    
    ![](https://gitlab.com/picbed/bed/uploads/4cf38587b1a3966be9752c137ecba827/PLA.png)
    
    * Communicate With [Bob](bob@nexpcb.com) face to face and he would give you guide how to use it in lab
3. If you find we are lack of material. You can find material information online or offline supplier ,and then fill the form in [google sheets](https://docs.google.com/spreadsheets/d/16YrZSg6tmvkSPjY2PUaWezGl4p_KxTKG7eFvPwa2ddA/edit?usp=sharing). Then inform Bob face to face or [email](bob@nexpcb.com), he would arrange purchasing.



## EE material

|  Material   | Link  |Parameter|
|  :----: | :----:  |:----:  |
| Wire| [Taobao](https://detail.tmall.com/item.htm?id=563127070857&spm=a1z09.2.0.0.d9762e8dXaCkKD&_u=v1kvrthkabc2)|Green0.4mm2, Blue0.4mm2,Black0.4mm2,Red0.4mm2,RV1.5mm2  |


## ME material

The following materil is the we own in lab

|  Material   | Link  |Parameter|
|  :----: | :----:  |:----:  |
| 3D printer PLA |[esun](https://detail.tmall.com/item.htm?spm=a230r.1.14.37.3d6a4993wLohQj&id=551633653327&ns=1&abbucket=4) | 1.75mm PLA White|
|Cobalt Drill |[Shanggong-Tianmao](https://detail.tmall.com/item.htm?id=44331981441&spm=a1z09.2.0.0.d9762e8d3607rF&_u=v1kvrthk6983) | 1.5,2,2.5,2.6,2.8,3,3.2,3.8,4,4.2,5|
|High speed Drill |[Komax-Tianmao](https://detail.tmall.com/item.htm?spm=a1z0d.6639537.1997196601.4.24a07484cTtDFV&id=585791040233) | 6,8,10,12|


We would update more detail soon
*****

#### Update time:
* V1:20200305
* V2:20200731
