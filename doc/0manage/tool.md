



# Tool

We classic our tool in lab as hand tool ,electric tool, software

## Electric tool 
Solder
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-131105.png)
multimeter
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-135637.png)
power
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-140817.png)

## EE material
 Solar  wire
 ![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-132411.png)
 Soldering Iron Tips
 ![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-132808.png)
 soldr paste
 ![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/soldertip.png)
## ME tool


  Laser cutter  Wuhan SUNIC （power 130w，working size1300x900mm）
  ![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WechatIMG242.jpeg)


 CAMM-1 GS-24 as vinyl cutter 

 ![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221230-122122.png)

## ME material

3M 2630 used in CAMM-1 GS-24 as vinyl cutter ,vinyl cutter can cutter film and as a part of tape.
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/3m.png)
Acrylic used in laser cutter. The material have different kind of thickness，we own 2mm，3mm，4mm，5mm transparent board.For color ，we own transparent color board（red，blue，green，yellow）
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-125543.png)
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221231-125749.png)


## Software

*****

#### Update time
* 20200305
