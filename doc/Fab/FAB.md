# Fab Lab
The Fab Lab Network is an open, creative community of fabricators, artists, scientists, engineers, educators, students, amateurs, professionals, of all ages located in more than 120 countries in approximately 2,000 Fab Labs. From community based labs to advanced research centers, Fab Labs share the goal of democratizing access to the tools for technical invention. This community is simultaneously a manufacturing network, a distributed technical education campus, and a distributed research laboratory working to digitize fabrication, inventing the next generation of manufacturing and personal fabrication
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221028-092100@2x.png)
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221028-092834@2x.png)

## 1 Aim
#### 1.1 The Fab Charter
*   ***What is a fab lab?***  Fab labs are a global network of local labs, enabling invention by providing access to tools for digital fabrication
* ***What’s in a fab lab?*** Fab labs share an evolving inventory of core capabilities to make (almost) anything, allowing people and projects to be shared
* ***What does the fab lab network provide?*** Operational, educational, technical, financial, and logistical assistance beyond what’s available within one lab
* ***Who can use a fab lab?*** Fab labs are available as a community resource, offering open access for individuals as well as scheduled access for programs
* ***What are your responsibilities?***
    * ***safety:*** not hurting people or machines
    * ***operations:*** assisting with cleaning, maintaining, and improving the lab
    * ***knowledge:*** contributing to documentation and instruction

* ***Who owns fab lab inventions?*** Designs and processes developed in fab labs can be protected and sold however an inventor chooses, but should remain available for individuals to use and learn from
* ***How can businesses use a fab lab?*** Commercial activities can be prototyped and incubated in a fab lab, but they must not conflict with other uses, they should grow beyond rather than within the lab, and they are expected to benefit the inventors, labs, and networks that contribute to their success
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221028-100621@2x.png)
#### 1.2 Fab Foundation and mission
* [Fab foundation ](http://www.fabfoundation.org) is formed in 2009 to facilitate and support the growth of the international fab lab network as well as the development of regional capacity-building organizations. The Fab Foundation is a US non-profit 501(c) 3 organization that emerged from MIT’s Center for Bits & Atoms Fab Lab Program. 
* Mission:Fablab's mission is to provide access to the tools, the knowledge and the financial means to educate, innovate and invent using technology and digital fabrication to allow anyone to make (almost) anything, and thereby creating opportunities to improve lives and livelihoods around the world. Community organizations, educational institutions and non-profit concerns are our primary beneficiaries.
  
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221028-093649@2x.png)


#### 1.3 Businesses emerging from fab labs and the digital fabrication network 

* [FormLab (Desktop SLA 3D printer)](https://formlabs.com/);
* [MakerBot (Desktop FDM 3D printer)](https://www.makerbot.com)
* [www.modk.it](http://www.modk.it/);
* [Ponoko ( Designer and Robot fast produce customer's need )](https://www.ponoko.com/);
* [Ultimaker (Desktop FDM 3D printer and open source  slice software)](https://ultimaker.com);
* [FabFi:an open source wireless network for $60 per node](https://www.geek.com/chips/fabfi-an-open-source-wireless-network-for-60-per-node-1395747/);
* Remake Electric;
* [Snap](https://www.snap.com/);
* [Nifty MiniDrive:An easy to use device that allows anyone to quickly and simply increase the available memory in their MacBook computer.]( http://www.kickstarter.com/projects/1342319572/the-nifty-minidrive)



## 2 Fab Academy
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221028-093801@2x.png)

## 3 Core content   

  
FAB academy is the key of fablab. this year's schedule in the [following link](http://fab.academany.org/2018/schedule.html).
  ***In every week's cycle:***
  
  * Wednesday:Neil introduce the general of content
  * Weekend:Local lab paractice
  * Monday:recitation(introduce some culture and practice about FAB)
  * Tuesday: area review:China(include Taiwan),Japan,South Korea)
  * Wednesday:Global review(communicate with 4-5 students about week's work and finial project)
    

  
  
  
  
  
  
  
## 4 Community :Attender and Organizer ,Global  net and China net, fab city,fab14+
#### 4.1 Net
[There are 1271 labs in the world](https://www.fablabs.io/labs/map),24 labs in China has register fablab, [more detail in the link](https://www.fablabs.io/labs?country=cn)

#### 4.2 2018 
There are 252 students attend the fab Academy in 64 labs. In China there are 9 students in 3labs(Shanghai,Shenzhen,Beijing)
#### 4.3 China attender
In the history of China, there are about 25 attenders. The background is about maker space manager,  STEAM teacher, startup company manager.
#### 4.4 Fab 14+
Fablab annual meet
![](https://ws2.sinaimg.cn/large/006tNc79ly1fr4iu5woryj31kw0puwkn.jpg)
![](https://ws2.sinaimg.cn/large/006tNc79ly1fr4iu78no2j31kw0x7tej.jpg)


#### 4.5 Fab city


The Fab City project was launched in 2011 at the FAB7 conference in Lima by the ***Institutd’Arquitectura Avançada de Catalunya, the MIT Center for Bits and Atoms, the Fab Foundation and the Barcelona City Council***

FAB City is a new urban model of transforming and shaping cities that shifts how they source and use materials from ‘Products In Trash Out’ (PITO) to ‘Data In Data Out’ (DIDO). This means that more production occurs inside the city, along with recycling materials and meeting local needs through local inventiveness. A city’s imports and exports would mostly be found in the form of data (information, knowledge, design, code).

There are 18 cities attend the project:

* USA(Boston,Cambridge,Somerville,Sacramento,Detroit);
* France(Toulouse,Occitane Region,Paris )
* China:Shenzhen
* South Africa:Ekurhuleni
* India:The state of Kerala
* The country of Georgia
* Netherlands:Amsterdam
* The state of Bhutan
* Chile:Santiago



 
## 5 Develop
Now Fablab  do the work about basic knowledge,  Innovation, design, prototype. 
They are also practice in Fab city,  [manufacture](https://make.works/), [market](http://market.fablabs.io/)

## 6 how to set up a Fablab
1. [Guide and register ](http://www.fabfoundation.org/index.php/setting-up-a-fab-lab/index.html)
2. [hardware] (https://www.fablabs.io/machines):All Fab Labs share common tools and processes in order to build a global network, a distributed laboratory for research and invention. Here you can find and add all the Machines available in the global Fab Lab Network, a global database of technologies locally available
3. [Layout](https://a360.co/2wmHVpj)
<iframe src="https://myhub.autodesk360.com/ue28cacf9/shares/public/SHabee1QT1a327cf2b7a64135bb75dc3708d?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>




## 7 Reference

* **Locations**  [ https://www.fablabs.io/labs/map ](https://www.fablabs.io/labs/map)
* **Foundation**[ http://www.fabfoundation.org ](http://www.fabfoundation.org)
* **Academy**  [ http://fabacademy.org ](http://fabacademy.org),[Fab academy 2018](http://fabacademy.org/2018/)
* **Events**  [ http://fabevent.org ](http://fabevent.org)
* **Fab cities**  [ http://fab.city ]( http://fab.city)
* **Video**  [ http://ng.cba.mit.edu/show/video/16.08.fablabs.mp4 ](http://ng.cba.mit.edu/show/video/16.08.fablabs.mp4)
* **Talk**  [ https://www.ted.com/talks/neil_gershenfeld_on_fab_labs ](https://www.ted.com/talks/neil_gershenfeld_on_fab_labs)
* **Presentation**  [ http://ng.cba.mit.edu/show/script/17.09.fab.show.html ](http://ng.cba.mit.edu/show/script/17.09.fab.show.html)
* **Article**  [ https://www.foreignaffairs.com/articles/2012-09-27/how-make-almost-anything ](https://www.foreignaffairs.com/articles/2012-09-27/how-make-almost-anything)
* **Book**  [ http://designingreality.org/ ](http://designingreality.org/)
* **Inventory**  [ http://fab.cba.mit.edu/about/fab/inv.html ](http://fab.cba.mit.edu/about/fab/inv.html)
* **Charter**  [ http://fab.cba.mit.edu/about/charter ](http://fab.cba.mit.edu/about/charter)
* **Cloud**  [ https://gitlab.fabcloud.org ](https://gitlab.fabcloud.org)
* **Research**  [ http://cba.mit.edu/events/13.03.scifab ](http://cba.mit.edu/events/13.03.scifab) 
* **Hardware**  [ http://mtm.cba.mit.edu ](http://mtm.cba.mit.edu) 
* **Software**  [ http://mods.cba.mit.edu ](http://mods.cba.mit.edu)
* **[Academy Key Personnel(Guru and Instructor)]**(http://fabacademy.org/2018/docs/FabAcademy-Handbook/academy_roles_and_key_personnel.html)

* Reference fablab 
  * [Ars Electronica Center](http://www.ireneposch.net/fablab/)

*****
Update time: 2019.12
Update time:2024.2


