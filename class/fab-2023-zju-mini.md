Hi everyone.
The following is our schedule for 8 weeks，start from 19 Sepember.



| Date        | Course Content           |  
| ------------- | :-----:|	
| 0919      | Introduction of design engineering | 
| 0926      | Project management      |  
| 1010 |Computer aided design   |   
| 1017     | 3D printing & laser cutter  | 
| 1024 |  Arduino      | 
| 1031     |  Interface application programming  | 
|1107|   Q&A  | 
|1128  |Final presentation   | 


## Our evolution method is the following

Daily homework full score is 70


| Course Content       | Full Score           |  
| ------------- | :-----:|	
| Project management  | 15   | 
| Computer aided design  |  10    |  
| Arduino   | 15  | 
|  Interface application programming  |  10   |  
|   3D printing & laser cutter |  20  |  
 



Final presentation full socre is  30 


| Evolutation aspect       | Full Score           |  
| ------------- | :-----:|	
| SDGs  |  6  | 
| Innovation |6      | 
| Market analysis | 6     | 
| Key tech analysis | 6     | 
| Material and how to make it | 6     | 




##  [Fivevolution](https://nexmaker-fab.github.io/2023zjudemini-hi1/#/)

| Course Content   |  Question        |  Update time   | Condation | 
| ------------- | :-----:|	 :-----:|	 :-----:|	
|   Project management   | ~~1.[no link for person in about us page](https://nexmaker-fab.github.io/2023zjudemini-hi1/#/md/about_us)~~ <br>~~2.wrong link in  翁诗彤 and 叶润 for github page in the bottom~~ <br>~~3. all English no mix~~<br>~~4.before"Ensure Source and Branch have been setting",you need build repository~~ <br>~~5. mention the problem and how to deal with them "page build and deployment"put  all the _config.yml on web  and highlight the minima~~<br>~~6. wrong  for  githubdestop with github deployment during push~~<br>~~7.the file relationship in fold especial _sidebar.md and _navbar.md~~<br>~~8.wrong information for _sidebar.md and _navbar.md~~<br>~~9.mention the function for navbar,sdiebar,coverpage~~<br>~~10.mention the real information for personal introduce and video in the web page~~<br> ~~11.how to write your first document~~<br>~~12.mention your final projec idea~~<br>~~13.how to collaborator team~~<br>~~mention the difference of two repositories(the different between wengstA/YourName with 2023zjudemini-hi1)~~ |20231107 |14|
|   Computer aided design   |~~1.plugin need more detail([from fusion360appstore](https://apps.autodesk.com/FUSION/en/Home/Index))~~<br>~~2.no joint~~<br> ~~3. motion realtionship need think~~<br>~~4. simple parameter design~~<br>~~5.how to make the first sketch~~<br>~~6.how to draw a Mechanical Drawing in software~~<br>~~7.everyone at least one model~~<br>~~8.no web link for 3Dmodel review~~<br>9.joint~~before motion~~<br>10.engineering need diammeter replace radius |20231127| 9| 
|  3D printing & laser cutter    |~~1.3d print software layout the model~~ <br>~~2.we just need one  nozzle~~<br>~~3.software parameter different with 3d printing parameter at least tempature~~<br>~~4.how to use laser cutter(open machine,transport data,running machine~~) <br>~~5.wrong picture for laser cutter running~~<br>~~6. try to find some new application and research~~<br>~~7.Use laser cutter to process your project~~<br>~~8.Assembe some laser cutter parts together~~<br>~~9.Assemble with your arduino project~~<br>~~10.video from screenshot replace mobile phone~~<br>~~11.Statistics on cutting test results~~<br>~~12.check the quality for " Set different parameters (power/speed) with different colors in Laser CAD V8.12~~<br>~~13.wrong picture (degree)for "Open the Laser cutting machine to location and border running"~~<br>~~14.open fume extractor before run the machine~~|20231127 | 19|   
|  Arduino    |~~1.Our Focusing Open Source Project with picture~~<br>~~2.[to do at least one arduino case with tinkercad connect drawing](https://www.tinkercad.com/) ,physical connect,coding,gif for result~~<br>~~3.the bilibili gif(function)~~<br>~~4.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~<br>~~5.in tinkercad,the line need vertical &horzon~~<br>~~6.the project need input and output at least~~<br>7. the first tinkercad picture is wrong<br>8.raspberry need information for connection picture and how to run it<br>basic informaion about opencv |20231127| 13| 
|   Interface application programming    | |20231107|9|   
|  Final presentation     | | 20231128| 28.5 |  

## [Fivist](https://nexmaker-fab.github.io/2023zjudemini-hi2/)

| Course Content   |  Question        |  Update time   | Condation | 
| ------------- | :-----:|	 :-----:|	 :-----:|	
|   Project management   |~~1.mention the file relation in fold"_config.yml"~~<br>~~2. mention  where can we get or learn more Theme~~<br>3.how to setting navbar and link<br>4.how to write your first document including link, tilte,picture<br>~~5.clone,pull ,push to github,~~<br>~~6. mention   your final projec idea~~<br>7.how to collaborator team<br>~~8.how to open terminal~~<br>~~9.  config.yml not config.ym1~~ |20231116|12| 
|   Computer aided design   |~~1.at least one part need every step~~<br>~~2.Plugin [from fusion360appstore](https://apps.autodesk.com/FUSION/en/Home/Index)~~<br>~~3. step by step for motion (how to setting)~~<br>~~4.no parameter desgin basic~~<br>~~5.need clear picture~~|20231119 |10 | 
|  3D printing & laser cutter   | ~~1.3d print software layout the model~~ <br>~~2.wrong temperature for "Comparison of results between two printouts"~~<br>~~3. record for team‘s own model for 3d printing~~<br>~~4.how to use laser cutter(open machine,transport data, running machine~~) <br>~~5. try to find some new application and research~~<br>~~6.Assemble with your arduino project~~<br>~~7.video from screenshot replace mobile phone~~<br>8.wrong picture for "Open the Laser cutting machine to location and border running"<br>~~9.open fume extractor before run the machine~~ | 20231030|17 |  
|  Arduino    | ~~1.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~<br>~~2.need input&output at least~~<br>3.just need one pciture for input &output（no fritzing and ws2812）<br>4. tinkerCAD line need vertical and horizon<br>5.Adafruit_NeoPixel.h just need one|20231127|12|  
|   Interface application programming    |~~1.check reference result~~<br>~~2.arduino connection line need horizon or vertical~~ |20231127|9|  
|  Final presentation     | | 20231128| 27.5 |  

******
#### Update time 
  * 20230918:update schedule












