As a part of Design Engineering,This workshop was lanched for ZJU industry design master.
The following is the schedule.

| Date        | Course Content           | 
| ------------- | -----:|	
| 9.17      | Introduction of design engineering | 
| 9.24      | Project management      |  
| 10.15 | Open source hardware and Arduino basic  |  
| 10.22      | Arduino (input devices)     |  
| 10.29 | Arduino (output devices)      |  
| 11.5     | Arduino IOT     |  
| 11.12 | Q&A      |  
|11.19     |Intelligent materials     |  
| 11.26 | Computer aided design      |  
| 12.3     | 3D printing      |  
| 12.10 | CNC machine      |   
| 12.17     | Midterm presentation ，Q&A     |  
| 12.24 | Computer-controlled cutting      |  
| 12.31      | Interface application programming  |  
| 1.7 | Video editing      |  
| 1.14      | Introduction of patent application |  
| 1.21 | Q&A     |  
| 1.28 | Final presentation    |  

60 students form 11 groups. The following is the team‘s link
* [924](http://team-924.gitlab.io/zjucst924/)
    * [924 Final project](class/zju2020/924.md)
  * [Duck Dog](https://1909097669.wixsite.com/duck-dog)
  * [Terminus](http://www.0xing.cn/home/blog/)
  * [D&L](http://dlight.designist.cn)
  * [Happyplanet](https://happyplanet196.wixsite.com/happyplanet)
    * [happly planet Final project](class/zju2020/happlyplanet.md)
  * [2.4GHZ](http://49.235.203.145)
  * [Coffe Bit](https://manatee257.github.io)
    * [Coffe Bit Final project](https://gitlab.com/nexmaker/zjunb-fab-01/video/-/blob/main/CoffeBit%E5%B0%8F%E7%BB%84-%E8%AE%BE%E8%AE%A1%E5%B7%A5%E7%A8%8B%E5%AD%A6.mp4)
  * [33Designer](https://yyniao.github.io)
    * [Final project](class/zju2020/33design.md)
    * [Video](https://gitlab.com/nexmaker/zjunb-fab-01/video/-/raw/main/33designer.mp4)
  * [YYclub](http://yyclub.designist.cn)
    * [YYclub Final project](class/zju2020/yyclub.md)
    * [Video](https://www.bilibili.com/video/BV1Z54y1s77x/?spm_id_from=333.337.search-card.all.click&vd_source=c4398c21423cc289db276fef26f1f480)
  * [Morning](https://1348328828.wixsite.com/mysite-2)
    * [Final project &vidoe](class/zju2020/morning.md)
  * [Natural](http://natural.designist.cn) 
    * [Natural Final project](class/zju2020/natural.md)
