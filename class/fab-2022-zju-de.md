As a part of Design Engineering 2022,This workshop was launched for ZJU industry design master.
40 students form 8 groups.
This project was launched in Sept 2022
We would list some key points every group need improve before final presentation.
The following is the schedule.








| Date        | Course Content           |  
| ------------- | :-----:|	
| 9.22      | Introduction of design engineering | 
| 9.29      | Project management      |  
| 10.8 | Computer aided design    |  
| 10.13     | Open source hardware and Arduino basic  | 
| 10.20 |     Arduino (output devices)    | 
| 10.27     |  Arduino (input devices)    | 
|11.3 |  CNC machine     | 
|11.10    |  Interface application programming   |  
| 11.17 |  3D printing   |  
| 11.24     |QA        |  
| 12.1|  Computer-controlled cutting      |   
| 12.8     | Midterm presentation ，Q&A     |  
| 12.15 |  Intelligent materials （zhou）    |  
| 12.22      | Arduino IOT  |  
| 12.29 |  Q&A     |  
| 1.5    | Final presentation  |  




#### [the Red Gang Tailor's](https://nexmaker-fab.github.io/2022zjude1-team5/#/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|Done|
| Project management      |~~1.check navbar~~<br>~~2.check link in "about"~~ <br>3.tell the problem"Win PC and failed "<br>~~4.introduce about style.css~~<br>~~5.font size a liitle bigger~~<br>6.mention how to manage github reopsitory and web page;<br>7. English at least |20221122|middle| 
| Computer aided design    | 1.confirm the sketch from blue to black <br>2.the command explain about revolve;<br>array setting method;<br> every one need at least one model ,and then assemble ,motion it ; |20221122|doing|
|  Arduino    | need your own homework,not just class test |20221122|doing|
|  CNC machine     |  |||
|  Interface application programming   |need processing & arduino practice（own not class demo） |20221223|high| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | |||  


#### [doubleQ](https://nexmaker-fab.github.io/2022zjude1-doubleQ/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |more detail about web build<BR> 1. from "LodaSidebar:true" to "LoadSidebar:true "<br>2.screenshot for key step<br>3.English at least<br>4.exchange data from loal to github"<br>5.simple introduce "package.json and package-lock.json"<br>6.novelai method in new page not in team introduce|20221122|doing| 
| Computer aided design    | 1.show assemble method<br>2. motion <br>3. drawing method<br>show conflict in same word in "修改-更改参数"|20221122|doing|
|  Arduino    | coding,thinkercad connection, real connection,gif for 2case |20221122|doing|
|  CNC machine     |  |||
|  Interface application programming   | show every step result  |20221122|doing| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | |||  

#### [BIG5](https://nexmaker-fab.github.io/2022zjude1-BIG5/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |some screenshot for   deploy<br>the mean of " hexo g "and "hexo s "|20221122|doing| 
| Computer aided design    | 1.confirm the size and postion for the first sketch<br>2.choice axis and profile for revolve <br>3.from "repeat the steps to create the rest wheels" to assemble ,in the meantime show the browser, we need build component to replay just in one body ,then assemble it<br>4. show how to add wood texture for windows<br>5.lack ebable  motion test |20221122|doing|
|  Arduino    |  |20221122|well|
|  CNC machine     |  |||
|  Interface application programming   | explore new tool similar with processing<br>Do one demo in processing which can use mouse or keyboard to interactive |20221122|doing| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | |||  

#### [SixGod](https://nexmaker-fab.github.io/2022zjude1-SixGod/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |1.English at least<br> 2.sidebar method|20221122|doing| 
| Computer aided design    | 1.show sketch button<br>2.extrude detail<br>3.wrong link for proe |20221122|doing|
|  Arduino    | some case with output |20221122|doing|
|  CNC machine     |  |||
|  Interface application programming   |1. show final coding<br>2.try ps5.js mode in processing<br>3. try to find some tool simialr with processing  |20221122|doing| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | ||| 

#### [ALL RIGHT](https://markshaw1234.github.io/2022zjudem-team4.github.io/index.html)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |1.coding highlight not just picture<br>2.english at least<br>3.step by step introduce how to make it|20221223|doing| 
| Computer aided design    |1.design detail<br>2. what's the mean of "添加驱动参数"<br> 3.solidwors don't contain fusion information <br>4.design one model for every step at least <br> 5.introduce assembly method <br> 6.need command for add material not just result<br>7. add fuison link which can preview 3d model in web|20221227|doing|
|  Arduino    |  1.thinkercad connect for "超声波测距，LCD1602显示" and real gif<br>~~2.coding highlight~~<br>3.in arduino& processing,need arduino part information(coidng ,connect)<br>4.thinkercad connect , real connect and final gif for "温湿度传感实验"|20221223|doing|
|  CNC machine     |  |||
|  Interface application programming   |  ||| 
|  3D printing   | 1.need machine setting and running information<br>2.need detail for post processing  |20221223|doing|
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | |||  
#### [Peopl3 Drag Per2on](https://nexmaker-fab.github.io/2022zjude1-peopl3-drag-per2on/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |1.Team introduce<br>2.no chinese in coding for markdown<br>3.how to upload data|20221122|doing| 
| Computer aided design    |1.more screenshot for keystep<br>2.assemble method<br>3.motion method |20221122|doing|
|  Arduino    |  |||
|  CNC machine     |  |||
|  Interface application programming   |  ||| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | ||| 
#### [SoFarSoGood](https://nexmaker-fab.github.io/2022zjude1-SoFarSoGood/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |20221122|done|
| Project management      |English at least<br>2.check the link method"[本页面](https://www.nexmaker.com/doc/1projectmanage/github&docsify.html)"<br>data upload to github method|20221122|doing| 
| Computer aided design    | discuss problem face to face|20221122|doing|
|  Arduino    | coding highlight not picture |20221122|doing|
|  CNC machine     |  |||
|  Interface application programming   | home work for processing  |20221122|doing| 
|  3D printing   |   |||
|  Computer-controlled cutting      |  |||  
|  Intelligent materials （zhou）    |   ||| 
| Arduino IOT  |   ||| 
| Final presentation  | ||| 


******
#### update
* 20221007:build page
* 20221122:v1 evoluation