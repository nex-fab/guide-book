# NexMaker-Fab

We have online guidebook [www.nexmaker.com](www.nexmaker.com) and offline lab in [NexPCB](www.nexpcb.com)，[ProFabX](www.profabx.com),etc.This lab would arrange training, hardware design, prototype making,project test,etc. We would try to push hardware innovation in our community one by one. In the meantime,We reference [Fablab](http://fabacademy.org/)  to build our lab.   Now we test beta version in our lab. We would open after test the function 
Nov 30,2022,Bob success become [Fab Lab instructor](http://fabacademy.org/nodes/requirements.html#instructors-application-process) and Fab Lab Ningbo-NexMaker become[ Fab Academy 2023 node](https://fabacademy.org/2023/labs.html),so we would launch Fab Academy 2023 with the help of some friends.

NexMaker academy  offer knowledge and skill about the hardware  development especial prototype stage, and guide participant to practice. During the course , participant can system learn “how to make almost everything”, they would learn project management, mechanical design and manufacture, embedded development ,material, IT, etc. This course is team working, and it is a good opportunity for them to solve some [SDGs](https://sdgs.un.org/) problems.

It's cool if you can give us any advice. We would update the lab one by one. Thank you for your kindly support.
[Bob wu who graduate from fablab 2018](http://fab.academany.org/2018/labs/fablaboshanghai/students/bob-wu/)would support the fab according to  Fab Lab and real need.

This website contains lecture materials, cases and examples that will be further used for the practical teaching project of new automobile cab design based on human factor engineering (基于人因工程学的新型汽车驾驶室工业设计实践教学项目) . The project is funded by “The First Batch of 2021 MOE of PRC Industry-University Collaborative Education Program (Program No.202101042006, Kingfar-CES “Human Factors and Ergonomics” Program, details see in [link](http://www.moe.gov.cn/s78/A08/tongzhi/202108/t20210827_554722.html).


## Student attend the course

![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/2023.png)

<iframe src="https://www.google.com/maps/d/u/0/embed?mid=13td1Pd1stAQKWJ8JkCvZKgJXz3mI13o&ehbc=2E312F" width="100%" height="600"></iframe>



## Support
* [Thunder Zhang](https://www.linkedin.cn/incareer/in/thunder-zhang-3b4090b) &[NexPCB](https://www.nexpcb.com/)：support resource to run the version 1
* [Bala Wang](https://www.linkedin.cn/incareer/in/balawxd):support build nexmaker.com
* [Stan Chen](https://github.com/STANAPO)：advice for web build
* [Jun Xu](business@moto-eye.com)：advice in electric development
* [Vicky cheng](m900hk@gmail.com): CAD support and structural mechanical design
* [Bob Wu](bobwu@profabx.com) cofounder of NexMaker
  
## Update recording
* 20200304:build it
* 20200819:add XR summary
* 20210915：add zju2021 team group
* 20210921:email and fablab link
* 202101107：add zju2021 dm team group
* 20220127: add BP in tutorials,rebuild the structure of XR,build the structure of AGV
* 20221201:Fab Lab node
* 20240422: add zwu-interactives sysyems
